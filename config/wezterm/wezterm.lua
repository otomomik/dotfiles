local wezterm = require('wezterm')
local config = {}

-- colorscheme
config.color_scheme = 'iceberg-dark'

-- WSL
-- config.default_prog = { 'wsl.exe' }

return config


