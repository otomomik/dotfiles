local options = {
	encoding = 'utf-8',
	fileencoding = 'utf-8',
	shell = 'fish',
	expandtab = true,
	shiftwidth = 2,
	tabstop = 2,
	number = true,
	background = 'dark',
  clipboard = 'unnamed,unnamedplus',
}

for k, v in pairs(options) do
	vim.opt[k] = v
end

